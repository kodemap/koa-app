// module dependencies
var koa = require('koa');

// application setup
var app = koa();

// X-Response-Time
app.use(function *(next) {
  var start = new Date();
  yield next;
  var ms = new Date() - start;
  this.set('X-Response-Time', ms + 'ms');
});

// logger
app.use(function *(next) {
  var start = new Date();
  yield next;
  var ms = new Date() - start;
  console.log('%s %s - %s', this.method, this.url, ms + 'ms');
});

// response
app.use(function *(next) {
  yield next;
  this.body = 'Hello World';
});


module.exports = app;
